/* -*- mode: c++ -*-
   Atreus -- Chrysalis-enabled Sketch for the Keyboardio Atreus
   Copyright (C) 2018, 2019  Keyboard.io, Inc

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built"
#endif

#include "Kaleidoscope.h"
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"
#include "Kaleidoscope-FocusSerial.h"
#include "Kaleidoscope-Macros.h"
#include "Kaleidoscope-MouseKeys.h"
#include "Kaleidoscope-OneShot.h"
#include "Kaleidoscope-Qukeys.h"
#include "Kaleidoscope-SpaceCadet.h"

enum {
  MACRO_QWERTY,
  MACRO_VERSION_INFO
};

#define Key_Exclamation LSHIFT(Key_1)
#define Key_At LSHIFT(Key_2)
#define Key_Hash LSHIFT(Key_3)
#define Key_Dollar LSHIFT(Key_4)
#define Key_Percent LSHIFT(Key_5)
#define Key_Caret LSHIFT(Key_6)
#define Key_Ampersand LSHIFT(Key_7)
#define Key_Asterisk LSHIFT(Key_8)
#define Key_LeftParenth LSHIFT(Key_9)
#define Key_RightParenth LSHIFT(Key_0)

#define Control_Layer MoveToLayer(CONTROL)
#define Base_Layer MoveToLayer(BASE)

#define BrightnessDown Consumer_DisplayBrightnessDecrement
#define BrightnessUp Consumer_DisplayBrightnessIncrement
#define VolumeMute Consumer_Mute
#define VolumeDown Consumer_VolumeDecrement
#define VolumeUp Consumer_VolumeIncrement
#define MediaPrevious Consumer_ScanPreviousTrack
#define MediaNext Consumer_ScanNextTrack
#define MediaPlay Consumer_PlaySlashPause

#define WindowCenter LCTRL(LALT(Key_M))
#define WindowWest LCTRL(LALT(Key_LeftArrow))
#define WindowEast LCTRL(LALT(Key_RightArrow))
#define WindowNorth LCTRL(LALT(Key_UpArrow))
#define WindowSouth LCTRL(LALT(Key_DownArrow))
#define WindowNorthWest LCTRL(LSHIFT(Key_UpArrow))
#define WindowNorthEast LCTRL(LSHIFT(Key_RightArrow))
#define WindowSouthEast LCTRL(LSHIFT(Key_DownArrow))
#define WindowSouthWest LCTRL(LSHIFT(Key_LeftArrow))

#define LowerOrQuote LT(LOWER, Quote)
#define UpperOrLeftBracket LT(UPPER, LeftBracket)

enum {
  BASE,
  LOWER,
  UPPER,
  CONTROL
};

/* *INDENT-OFF* */
KEYMAPS(
  [BASE] = KEYMAP_STACKED
  (
       Key_Q        ,Key_W     ,Key_E              ,Key_R            ,Key_T
      ,Key_A        ,Key_S     ,Key_D              ,Key_F            ,Key_G
      ,SFT_T(Z)     ,CTL_T(X)  ,GUI_T(C)           ,ALT_T(V)         ,Key_B         ,XXX
      ,Key_Backtick ,Key_Minus ,Key_Equals         ,LowerOrQuote     ,Key_Backspace ,Key_Esc

                    ,Key_Y     ,Key_U              ,Key_I            ,Key_O         ,Key_P
                    ,Key_H     ,Key_J              ,Key_K            ,Key_L         ,Key_Semicolon
      ,XXX          ,Key_N     ,ALT_T(M)           ,GUI_T(Comma)     ,CTL_T(Period) ,SFT_T(Slash)
      ,Key_Tab      ,Key_Space ,UpperOrLeftBracket ,Key_RightBracket ,Key_Backslash ,Key_Enter
  ),

  [LOWER] = KEYMAP_STACKED
  (
       Key_Exclamation ,Key_At          ,Key_Hash      ,Key_Dollar   ,Key_Percent
      ,Key_1           ,Key_2           ,Key_3         ,Key_4        ,Key_5
      ,Key_LeftShift   ,Key_LeftControl ,Key_LeftGui   ,Key_LeftAlt  ,XXX             ,XXX
      ,XXX             ,XXX             ,XXX           ,XXX          ,Key_Backspace   ,Key_Esc

                       ,Key_Caret       ,Key_Ampersand ,Key_Asterisk ,Key_LeftParenth ,Key_RightParenth
                       ,Key_6           ,Key_7         ,Key_8        ,Key_9           ,Key_0
      ,XXX             ,XXX             ,Key_LeftAlt   ,Key_LeftGui  ,Key_LeftControl ,Key_LeftShift
      ,Key_Tab         ,Key_Space       ,XXX           ,XXX          ,XXX             ,XXX
  ),

  [UPPER] = KEYMAP_STACKED
  (
       Key_F1  ,Key_F2        ,Key_F3          ,Key_F4        ,Key_F5
      ,Key_F11 ,Key_F12       ,Key_PrintScreen ,Key_Insert    ,Key_Delete
      ,___     ,___           ,___             ,___           ,___            ,___
      ,___     ,___           ,___             ,___           ,___            ,___

               ,Key_F6        ,Key_F7          ,Key_F8        ,Key_F9         ,Key_F10
               ,Key_LeftArrow ,Key_DownArrow   ,Key_UpArrow   ,Key_RightArrow ,XXX
      ,___     ,___           ,___             ,___           ,___            ,___
      ,___     ,___           ,___             ,Control_Layer ,___            ,___
  ),

  [CONTROL] = KEYMAP_STACKED
  (
       BrightnessDown ,BrightnessUp ,VolumeMute    ,VolumeDown      ,VolumeUp
      ,XXX            ,XXX          ,MediaPrevious ,MediaPlay       ,MediaNext
      ,XXX            ,XXX          ,XXX           ,XXX             ,XXX          ,XXX
      ,XXX            ,XXX          ,XXX           ,XXX             ,XXX          ,Base_Layer

                      ,XXX          ,XXX           ,WindowNorthWest ,WindowNorth  ,WindowNorthEast
                      ,XXX          ,XXX           ,WindowWest      ,WindowCenter ,WindowEast
      ,XXX            ,XXX          ,XXX           ,WindowSouthWest ,WindowSouth  ,WindowSouthEast
      ,XXX            ,XXX          ,XXX           ,XXX             ,XXX          ,XXX
  )
)
/* *INDENT-ON* */

KALEIDOSCOPE_INIT_PLUGINS(
  EEPROMSettings,
  EEPROMKeymap,
  Focus,
  FocusEEPROMCommand,
  FocusSettingsCommand,
  Qukeys,
  SpaceCadet,
  OneShot,
  Macros,
  MouseKeys
);

const macro_t *macroAction(uint8_t macroIndex, uint8_t keyState) {
  switch (macroIndex) {
    case MACRO_QWERTY:
      // This macro is currently unused, but is kept around for compatibility
      // reasons. We used to use it in place of `MoveToLayer(BASE)`, but no
      // longer do. We keep it so that if someone still has the old layout with
      // the macro in EEPROM, it will keep working after a firmware update.
      Layer.move(BASE);
      break;
    case MACRO_VERSION_INFO:
      if (keyToggledOn(keyState)) {
        Macros.type(PSTR("Keyboardio Atreus - Kaleidoscope "));
        Macros.type(PSTR(BUILD_INFORMATION));
      }
      break;
    default:
      break;
  }

  return MACRO_NONE;
}

void setup() {
  QUKEYS(
    kaleidoscope::plugin::Qukey(0, KeyAddr(0, 0), Key_LeftGui)
  )

  Qukeys.activate();
  Kaleidoscope.setup();
  SpaceCadet.disable();
  EEPROMKeymap.setup(4);
}

void loop() {
  Kaleidoscope.loop();
}
